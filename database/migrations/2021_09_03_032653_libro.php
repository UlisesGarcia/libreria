<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Libro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('libros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 60);
            $table->string('autor', 60);
            $table->date('fechaPublicacion');
            $table->unsignedBigInteger('usuarioid')->nullable();
            
            $table->foreign('usuarioid')
            ->references('id')
            ->on('users')
            ->onDelete('set null');
            
            $table->unsignedBigInteger('categoriaid')->nullable();

            $table->foreign('categoriaid')
            ->references('id')
            ->on('categorias')
            ->onDelete('set null');

            $table->boolean('activo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('libros');
    }
}
