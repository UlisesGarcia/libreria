<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Libro
 *
 * @property $id
 * @property $nombre
 * @property $autor
 * @property $fechaPublicacion
 * @property $usuarioid
 * @property $categoriaid
 * @property $created_at
 * @property $updated_at
 *
 * @property Categoria $categoria
 * @property User $user
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Libro extends Model
{

    static $rules = [
      'nombre' => 'required',
      'autor' => 'required',
      'fechaPublicacion' => 'required',
      'usuarioid',
  ];

  protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','autor','fechaPublicacion','usuarioid','categoriaid'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categoria()
    {
        return $this->hasOne('App\Models\Categoria', 'id', 'categoriaid');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'usuarioid');
    }
    
    public function scopeName($query, $name){
        if($name)
            return $query->where('nombre', 'LIKE', "%$name%");
    }
}
