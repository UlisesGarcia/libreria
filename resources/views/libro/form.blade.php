<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('nombre') }}
            {{ Form::text('nombre', $libro->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
            {!! $errors->first('nombre', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('autor') }}
            {{ Form::text('autor', $libro->autor, ['class' => 'form-control' . ($errors->has('autor') ? ' is-invalid' : ''), 'placeholder' => 'Autor']) }}
            {!! $errors->first('autor', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('fecha Publicacion') }}
            {{ Form::date('fechaPublicacion', $libro->fechaPublicacion, ['class' => 'form-control' . ($errors->has('fechaPublicacion') ? ' is-invalid' : ''), 'placeholder' => 'Fechapublicacion']) }}
            {!! $errors->first('fechaPublicacion', '<div class="invalid-feedback">:message</p>') !!}
        </div>

        <label for="">Usuario prestado</label>
        <select name="usuarioid" id="usuarioid" class="form-control">
            <option value="">Ninguno</option>
            @foreach($usuarios as $usuario)
            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
            @endforeach
        </select>
        <br/>
        <label for="">Categoria</label>
        <select name="categoriaid" id="categoriaid" class="form-control">
            @foreach($categorias as $categoria)
            <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
            @endforeach
        </select>
        <br/>
    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>